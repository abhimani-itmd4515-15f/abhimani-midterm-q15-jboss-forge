package edu.iit.sat.itmd4515.abhimani.mp2.entities;

import edu.iit.sat.itmd4515.abhimani.mp2.SuperEntityUnit;
import edu.iit.sat.itmd4515.abhimani.mp2.EventState;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp2
 */
@Entity
@Table(name = "event_store")
@NamedQueries({
		@NamedQuery(name = "Events.retrieveAll", query = "SELECT e FROM Event AS e"),
		@NamedQuery(name = "Events.findById", query = "SELECT e FROM Event AS e WHERE e.PId=:EventId")})
public class Event extends SuperEntityUnit
		implements
			Comparable<Event>,
			Serializable {
	//COLUMNS
	@Column(name = "Title", nullable = false, length = 255, unique = true)
	private String Title;

	@Column(name = "Description", length = 2000)
	private String Description;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EBegin", nullable = false)
	private Date EBegin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EEnd", nullable = false)
	private Date EEnd;

	@Column(name = "AState", nullable = false)
	private EventState AState = EventState.Active;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CDate", nullable = false, insertable = true, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date CDate = new Date();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MDate", insertable = false, updatable = true, columnDefinition = "TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private Date MDate;

	//CONSTRUCTS
	public Event() {
		super();
	}

	public Event(String Title, String Description, Date EBegin, Date EEnd) {
		this.Title = Title.trim();
		this.Description = Description.trim();
		this.EBegin = EBegin;
		this.EEnd = EEnd;
	}

	public Event(String Title, String Description, Date EBegin, Date EEnd,
			EventState AState) {
		this.Title = Title.trim();
		this.Description = Description.trim();
		this.EBegin = EBegin;
		this.EEnd = EEnd;
		this.AState = AState;
	}

	//PROPERTIES
	public String getTitle() {
		return Title;
	}

	public void setTitle(String Title) {
		this.Title = Title.trim();
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description.trim();
	}

	public String getEBegin() {
		return EBegin.toString();
	}

	public void setEBegin(Date EBegin) {
		this.EBegin = EBegin;
	}

	public String getEEnd() {
		return EEnd.toString();
	}

	public void setEEnd(Date EEnd) {
		this.EEnd = EEnd;
	}

	public String getAState() {
		return AState.toString();
	}

	public void setAState(EventState AState) {
		this.AState = AState;
	}

	public String getCDate() {
		return CDate.toString();
	}

	public String getMDate() {
		return ((MDate == null) ? "-" : MDate.toString());
	}

	//IMPLEMENTATION
	@Override
	public int compareTo(Event el) {
		return (this.getTitle().compareTo(el.getTitle()));
	}

	//OVERRIDES
	@Override
	public boolean equals(Object el) {
		if (!(el instanceof Event))
			return false;
		try {
			Event tstEvt = (Event) el;
			if (!(Integer.compare(this.hashCode(), tstEvt.hashCode()) == 0))
				return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		try {
			return ("/Entities.Events{Id:"
					+ this.getPid()
					+ ", Title:\""
					+ this.getTitle()
					+ "\", Description:{Length:"
					+ ((this.getDescription() == null) ? 0 : this
							.getDescription().length()) + "}, EBegin:\""
					+ this.getEBegin() + "\", EEnd:\"" + this.getEEnd()
					+ "\", State:" + this.getAState().toUpperCase()
					+ ", Create_Date:\"" + this.getCDate()
					+ "\", Last_Modified:\"" + this.getMDate() + "\"}");
		} catch (Exception ex) {
			ex.printStackTrace();
			return ex.toString();
		}
	}
}
