package edu.iit.sat.itmd4515.abhimani.mp2.test_entities;

import edu.iit.sat.itmd4515.abhimani.mp2.AbstractTestJUnit;
import edu.iit.sat.itmd4515.abhimani.mp2.entities.Department;
import edu.iit.sat.itmd4515.abhimani.mp2.entities.Event;
import edu.iit.sat.itmd4515.abhimani.mp2.entities.Venue;
import edu.iit.sat.itmd4515.abhimani.mp2.EventState;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp2
 */
public class Test6_Event
	extends AbstractTestJUnit{
    //CRUD - calls
    private int UPDATE_ID=0;
    private int DELETE_ID=0;

    @Override
    protected void create()
	    throws Exception{
	Event e1=new Event("International Student Orientation", "All F1 and J1 international students (both graduate and undergraduate) are required to attend International Student SOAR.", (new GregorianCalendar(2015, 8, 21, 9, 30, 0)).getTime(), (new GregorianCalendar(2015, 8, 21, 13, 0, 0)).getTime()),
		e2=new Event("ITA Tech Challenge", "In its 6th year, the ITA Tech Challenge is a programming and coding skills competition for students at targeted Midwest universities.", (new GregorianCalendar(2015, 9, 21, 17, 0, 0)).getTime(), (new GregorianCalendar(2015, 9, 21, 19, 30, 0)).getTime()),
		e3=new Event("Fall 2015 Career Fair", "If you are in need professional attire, OCL will have a clothing closet in the lower level of Hermann Hall.", (new GregorianCalendar(2015, 9, 24, 12, 40, 0)).getTime(), (new GregorianCalendar(2015, 9, 24, 16, 0, 0)).getTime()),
		e4=new Event("Party", "", (new GregorianCalendar(2015, 9, 28, 10, 30, 0)).getTime(), (new GregorianCalendar(2015, 9, 29, 3, 30, 0)).getTime(), EventState.Hold);
	em.flush();
	em.persist(e1);
	em.persist(e2);
	em.persist(e3);
	em.persist(e4);
	em.flush();
	UPDATE_ID=e4.getPid();
	DELETE_ID=e2.getPid();
	System.out.println("\n-->> 4 records inserted.\n");
    }

    @Override
    protected void retrieve()
	    throws Exception{
	List<Event> ss;
	ss=em.createNamedQuery("Events.retrieveAll", Event.class).getResultList();
	for(Event s:ss)
	    System.out.println(s.toString());
	System.out.println("\n-->> "+ss.size()+" records retrieved.\n");
    }

    @Override
    protected void update()
	    throws Exception{
	Event e;
	em.flush();
	e=em.createNamedQuery("Events.findById", Event.class).setParameter("EventId", UPDATE_ID).getSingleResult();
	if(e!=null){
	    e.setTitle("Homecoming Bog Party");
	    e.setDescription("-- This is an updated value --");
	    e.setAState(EventState.Active);
	}
	em.persist(e);
	em.flush();
	System.out.println("\n-->> 1 record updated : /Entities.Events{Department:\"College of Architecture\", Venue:\"The BOG\", Title:\"Homecoming Bog Party\", Description:\"-- This is an updated value --\", State:ACTIVE}\n");
    }

    @Override
    protected void delete()
	    throws Exception{
	Event e;
	e=em.createNamedQuery("Events.findById", Event.class).setParameter("EventId", DELETE_ID).getSingleResult();
	if(e!=null)
	    em.remove(e);
	System.out.println("\n-->> 1 record deleted : /Entities.Events{Department:\"School of Applied Technology\", Venue:\"McCormick Tribute Campus Center\", Title:\"ITA Tech Challenge\", Description:\"In its 6th year, the ITA Tech Challenge is a programming and coding skills competition for students at targeted Midwest universities.\", State:ACTIVE}\n");
    }
}
